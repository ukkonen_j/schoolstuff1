package kayttoPack;

import java.awt.BorderLayout;
import java.io.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Choice;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Kayttis extends JFrame {
	
	String puoli = "1";
	String nopeus;
	String etaisyys;
	String kiihdytys;
	String estelkm = "1";
	
	
	
	Yhteys yhteys = new Yhteys();
	
	// Luodaan k�ytt�j�rjestelm�n komponentit
	
	JTextPane tekstikentta = new JTextPane();
	private JPanel contentPane;
	private JTextField txtSpeed;
	JComboBox<String> comboBox = new JComboBox<String>();
	JLabel lblEsteidenLukumaaraRadalla = new JLabel("Esteiden lukumaara radalla");
	JTextPane txtpnEsteidenLukumaara = new JTextPane();
	JTextPane txtpnKiihdytysarvo = new JTextPane();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Kayttis frame = new Kayttis();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Kayttis() {
		
		yhteys = new Yhteys();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 415, 368);
		contentPane = new JPanel();
		
		
		
		
		tekstikentta.setBounds(10, 302, 380, 19);
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Luo USB -yhteyden k�ytt�j�rjestelm�n ja robotin v�lille
		
		JButton btnConnect = new JButton("Yhdista");
		btnConnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				yhteys.Connect();

                if (!yhteys.connected) {
                    tekstikentta.setText("USB yhteys luotu");
                } else {
                    tekstikentta.setText("Yhdistäminen epäonnistui");
                }
			}
		});
		
		btnConnect.setBounds(35, 30, 355, 25);
		contentPane.add(btnConnect);
		
		
		// puolen valinta -vetovalikko
		comboBox.addItem("Oikea");
		comboBox.addItem("Vasen");
		comboBox.setBounds(35, 108, 117, 24);
		contentPane.add(comboBox);
		
		
		
		comboBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				
				if(comboBox.getSelectedIndex() == 1 ){
					puoli = "2";
				}
				else {
					puoli = "1";
				}		
			}
		});
		
		// Arvojen l�hetys
		
		JButton btnSetspeed = new JButton("Aseta arvot");
		btnSetspeed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSetspeed.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				// tallennetaan komponenttien arvot muuttujiin ja l�hetet��n robotille
				
				nopeus = txtSpeed.getText();
				estelkm = txtpnEsteidenLukumaara.getText();
				kiihdytys = txtpnKiihdytysarvo.getText();
				
                try {
                    yhteys.dos.writeUTF("nopeus = "+nopeus+"\npuoli = "+puoli + "\nestelkm = "+estelkm+"\nkiihdytysarv = "+kiihdytys);
                    yhteys.dos.flush();
                   // tekstikentta.setText("Power asetettu");

                } catch (IOException c) {
                    c.printStackTrace();
                } finally {
                	try {
						yhteys.dos.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
                }

			}
		});
		
		btnSetspeed.setBounds(35, 240, 355, 25);
		contentPane.add(btnSetspeed);
		
		// Lis�� komponenttien alustusta
		
		txtSpeed = new JTextField();
		txtSpeed.setText("Nopeus");
		txtSpeed.setBounds(35, 171, 114, 19);
		contentPane.add(txtSpeed);
		txtSpeed.setColumns(10);
		
		
		
		JLabel lblSpeed = new JLabel("Nopeus");
		lblSpeed.setBounds(35, 154, 70, 15);
		contentPane.add(lblSpeed);
		
		
		contentPane.add(tekstikentta);
		
		
		txtpnKiihdytysarvo.setText("Kiihdytysarvo");
		txtpnKiihdytysarvo.setBounds(232, 108, 158, 19);
		contentPane.add(txtpnKiihdytysarvo);
		
		JLabel lblKiihdytysarvo = new JLabel("Kiihdytysarvo");
		lblKiihdytysarvo.setBounds(232, 86, 158, 14);
		contentPane.add(lblKiihdytysarvo);
		
		JLabel lblValitsePuoli = new JLabel("Valitse puoli");
		lblValitsePuoli.setBounds(36, 86, 88, 14);
		contentPane.add(lblValitsePuoli);
		
		
		lblEsteidenLukumaaraRadalla.setBounds(232, 154, 158, 15);
		contentPane.add(lblEsteidenLukumaaraRadalla);
		
		
		txtpnEsteidenLukumaara.setText("Esteiden lukumaara");
		txtpnEsteidenLukumaara.setBounds(232, 171, 158, 20);
		contentPane.add(txtpnEsteidenLukumaara);
		
		

		
		
		
	}
}
