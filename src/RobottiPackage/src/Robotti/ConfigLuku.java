package Robotti;
import java.util.*;
import java.io.*;

import lejos.nxt.*;

public class ConfigLuku {
	
	// Tällä luokalla luetaan config tiedosto, johon on asetettu halutut arvot käyttöjärjestelmästä
	
	public int luku(String a){
		int tulos = 0;
		File filename = new File("config.ini");
		String haku = a;
		Properties p;
		String string;
		
		
		try{
			InputStream is = new FileInputStream(filename);
			p = new Properties();
			p.load(is);
			
			
			string = p.getProperty(haku);
			tulos = Integer.parseInt(string);
			
		}
		catch(IOException e){
			System.out.println(e);
		}
		
		return tulos;
	}
	
	
}
