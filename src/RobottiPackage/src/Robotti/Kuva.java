package Robotti;

import java.io.File;
import java.io.FileInputStream;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import lejos.nxt.Button;
import lejos.nxt.LCD;

public class Kuva {

	Graphics g = new Graphics();
    final int SW = LCD.SCREEN_WIDTH;
    final int SH = LCD.SCREEN_HEIGHT;
	
    // Ajoluokasta lähetetään kuvannimi parametri (eli tiedosto joka sijaitsee robotissa), jonka mukaan muodostetaan kuva
    
    public void kuvanLisays(String kuvannimi) throws Exception
    {
        
        Image img = Image.createImage(new FileInputStream(new File(kuvannimi)));
        g.drawRegion(img, 0, 0, SW, SH, Sprite.TRANS_NONE, SW / 2, SH / 2, Graphics.HCENTER | Graphics.VCENTER);
        Button.waitForAnyPress();
    }
	
}
