package Robotti;

import lejos.nxt.*;
import lejos.util.*;

public class Seurailija implements Runnable  {

	Ajoluokka ajo = new Ajoluokka();

	ColorSensor light = new ColorSensor(SensorPort.S4);
	Stopwatch stopwatch = new Stopwatch();

	// asetetaan v�rien arvot aloituskalibroinnista joka tapahtuu VariKalibrointi -luokassa
	private int musta = VariKalibrointi.getMusta();
	private int valkoinen = VariKalibrointi.getValkoinen();
	private int blackWhiteThreshold = VariKalibrointi.getBlackWhiteThreshold();
	
	public boolean isRunning = ajo.mo.isRunning();
	private int aika;
	private int rajaValkoinen = 5; // Valkoisen raja-arvo +-
	private int rajaMusta = 6;  //Mustan raja-arvo +

	public synchronized void run() {


		light.setFloodlight(true);
		LCD.clear();
		LCD.drawString("Kierros alkaa 5", 0, 0);
		LCD.drawString("sekunnin", 0, 1);
		LCD.drawString("kuluttua.", 0, 2);

		// 5 Sekunnin viive
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LCD.clear();
		stopwatch.reset();


		while (ajo.mo.isRunning()) {

			LCD.drawInt(light.getLightValue(), 1, 1);
			aika = stopwatch.elapsed();

			// OIKEAN PUOLEN SEURAUS
			if (ajo.mo.getPuoli() == 1) {
				// Jos havaitaan este asetetun matkan p��ss�, kutsutaan esteenohitus -metodia
				if (ajo.ky.esteEdessa(22)) {

					ajo.mo.EsteenOhitus();

				}

				else {
						// V�risensorista saatavilla arvoilla robotti suuntaa viivaa pitkin (jos valkoinen isompi -> menn��n enemm�n keskelle etc.)
					if (light.getLightValue() > blackWhiteThreshold) {
							// Jos on jouduttu jo suuresti eroon keskikohdasta, kutsutaan jyrkk�� k��nn�st� liev�n sijaan
						if (light.getLightValue() <= (valkoinen + rajaValkoinen) && light.getLightValue() >= (valkoinen - rajaValkoinen)) {  

							ajo.mo.tiukkaVasen();
						} else {
							ajo.mo.Vasemmalle();
						}

					}
					// sama toisinp�in
					if (light.getLightValue() < blackWhiteThreshold) {

						if (light.getLightValue() <= (musta + rajaMusta)) { 									
							ajo.mo.tiukkaOikea();
						} else {
							ajo.mo.Oikealle();
						}

					}

				}

			}


			// VASEMMAN PUOLEN SEURAUS
			if (ajo.mo.getPuoli() == 2) {
				
				// Esteen ohitus

				if (ajo.ky.esteEdessa(22)) {

					ajo.mo.VasenEsteenOhitus();
					
					// 

					
				} else {
					if (light.getLightValue() > blackWhiteThreshold) {

						if (light.getLightValue() <= (valkoinen + rajaValkoinen) && light.getLightValue() >= (valkoinen - rajaValkoinen)) {
							System.out.println("VASEN");
							ajo.mo.tiukkaOikea();

						} else {
							System.out.println("VASEN");
							ajo.mo.Oikealle();

						}

					}
					
					// 

					if (light.getLightValue() < blackWhiteThreshold) {

						if (light.getLightValue() <= (musta + rajaMusta)) {
							System.out.println("VASEN");
							ajo.mo.tiukkaVasen();

						} else {
							System.out.println("VASEN");
							ajo.mo.Vasemmalle();

						}

					}
				}

			}
		}

		ajo.mo.Pysahdy();
		LCD.clear();
		LCD.drawString("Aika:", 0, 0);
		LCD.drawInt(aika / 1000, 0, 1);
		LCD.drawString("sekuntia.", 4, 1);
		Button.ESCAPE.waitForPress();

	}

}
