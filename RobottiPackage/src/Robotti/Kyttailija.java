package Robotti;

import lejos.nxt.*;
import lejos.util.*;
import lejos.robotics.*;


public class Kyttailija implements Runnable{

	UltrasonicSensor sonic = new UltrasonicSensor(SensorPort.S1);

	public void run() {
		// Tulostetaan n�yt�lle et�isyys l�himp��n edess�olevaan objektiin
		LCD.drawInt(sonic.getDistance(),3,0);

		// Pidet��n sensori p��ll� 
		sonic.continuous();

	}
		// Seurailija -luokasta l�hetet��n parametrilla arvo, jonka mukaan halutaan esteen ohituksen tapahtuvan
	public boolean esteEdessa (int g){

		if (sonic.getDistance() < g){
			return true;

		}
		else {

			return false;
		}
	}

}
