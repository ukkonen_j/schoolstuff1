package Robotti;

import lejos.nxt.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.*;

public class Moottori {
	
	
	ConfigLuku configluku = new ConfigLuku();
	
	Ajastin aj = new Ajastin();
	DifferentialPilot pilot = new DifferentialPilot(5.6f, 11f, Motor.B, Motor.C);
	ColorSensor light2 = new ColorSensor(SensorPort.S4);
	private int blackWhiteThreshold = VariKalibrointi.getBlackWhiteThreshold();  //saadaan viivan arvo alkukalibroinnista
	private int nopeus = configluku.luku("nopeus"); // Luetaan config tiedostosta asetetut arvot
	private int nopeus2 = (int) Math.round(nopeus * 0.95); //Oltava pienempi kuin nopeus, jotta korjaa aina radalle päin metodia kutsuttaessa.
	private int tiukKaanos = nopeus / 2; //kuinka nopeasti kääntyy radan mutkissa ja jos hukkaa radan.
	private int accele = 4000; //kuinka nopeasti kiihtyy tavoite nopeuteen, max 6000 silloin pysähtyy kuin seinään ja mönkkäri lyö anturin maahan.
	private int suoraaccele = 5000;
	private int jyrkkaAcce = 3500; //eri kiihtyvyys eri tilanteisiin.
	private int hidasAccele = 250;
	private int nopeeAccele =400;
	private int mutkaNopeus = 5;
	private int kiihdArvo = configluku.luku("kiihdytysarv"); //mitä suurempi sitä hitaampaa kiihtyy suoralla. jos liian nopeaa hukkaa keskiviivan. aika.elapsed /kiihdArvo
	public int ajastinKerroin=0; //alustettu ajastin kerroin. Toimii kellona.(ajastinKerroin +  aj.aika.elapsed()/kiihdArvo)
	private int puoli = configluku.luku("puoli"); // Luetaan config tiedostosta asetetut arvot, 1 = oikea 2 = vasen, 
	private int este = 0;
	private int esteidenLkm = configluku.luku("estelkm"); // Luetaan config tiedostosta asetetut arvot
	public boolean isRunning = true;
	

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public int getPuoli() {
		return puoli;
	}

	public void setPuoli(int puoli) {
		this.puoli = puoli;
	}

	public int getNopeus() {
		return nopeus;
	}

	public void setNopeus(int nopeus) {
		this.nopeus = nopeus;
	}

	public void Oikealle() {
		
		System.out.println(nopeus2);
		
		//Aina metodia kutsuttaessa kiihtyy oikealle (ajastinKerroin)
		//N�in pysyy tiell� kun vasen tekee saman.
		ajastinKerroin = aj.aika.elapsed() / kiihdArvo;
		Motor.B.setAcceleration(suoraaccele);
		Motor.C.setAcceleration(suoraaccele);
		LCD.clear();
		Motor.B.setSpeed(nopeus + ajastinKerroin);
		Motor.C.setSpeed(nopeus2);
		Motor.C.forward();
		Motor.B.forward();
		LCD.drawString("Oikealle", 1, 2);
		LCD.drawInt(Motor.B.getSpeed(), 1, 5);
	}

	public void tiukkaOikea() {

		System.out.println(nopeus2);
		//K��ntyy tikasti oikealle. JyrkkaAccele jotta toinen rengas jarruttaa hieman hitaammin.
		// C moottori ei stoppaa koska stoppi pys�ytt�� renkaan kuin sein��n eik� accele toimi.
		aj.aika.reset();
		Motor.B.setAcceleration(accele);
		Motor.C.setAcceleration(jyrkkaAcce);
		LCD.clear();
		Motor.B.setSpeed(tiukKaanos);
		Motor.C.setSpeed(mutkaNopeus);
		Motor.B.forward();
		Motor.C.forward();
		LCD.drawString("tiukkaoikea", 1, 3);

	}

	public void Vasemmalle() {
		//Sama kuin oikea mutta vasemmalle, kiihtyy mitä kauemmin metodia kutsutaan.
		//Kello resettaa vasta kun hukkaa vasemman tai oikean metodin.
		//t�m� saa aikaan kiihtyvyyden suoralla
		ajastinKerroin = aj.aika.elapsed() / kiihdArvo;
		Motor.B.setAcceleration(accele);
		Motor.C.setAcceleration(suoraaccele);
		LCD.clear();
		Motor.B.setSpeed(nopeus2);
		Motor.C.setSpeed(nopeus + ajastinKerroin);
		Motor.C.forward();
		Motor.B.forward();
		LCD.drawString("vasemmalle", 1, 4);
		LCD.drawInt(Motor.C.getSpeed(), 1, 5);
	}

	public void tiukkaVasen() {
		//Sama kuin tiukkaOikea mutta vasemmalle

		aj.aika.reset();
		Motor.B.setAcceleration(jyrkkaAcce);
		Motor.C.setAcceleration(accele);
		LCD.clear();
		LCD.drawString("TiukkaVasen", 1, 5);
		Motor.B.setSpeed(mutkaNopeus);
		Motor.C.setSpeed(tiukKaanos);
		Motor.C.forward();
		Motor.B.forward();

	}

	public void EsteenOhitus() {
		light2.setFloodlight(true);
		LCD.clear();
		pilot.stop();
		Sound.beep();
		
		// Esteiden lukum��r�, kun k�ytt�j�rjestelm�st� asetettu raja toteutuu, robotti pys�htyy.

		if (este >= esteidenLkm){
			pilot.stop();
			setRunning(false);
		} else {
			este++;
			pilot.setTravelSpeed(20);
			LCD.drawString("Ohitetaan", 0, 0);
			
			// Kun liikutaan pilotin avulla, pys�ytet��n muut threadit

			while(pilot.isMoving())Thread.yield();

			pilot.setAcceleration(hidasAccele);

			// v�ist�
			pilot.reset();
			pilot.rotate(-45);
			pilot.travel(40);
			pilot.reset();
			pilot.setRotateSpeed(200);
			pilot.rotate(90);
			pilot.setAcceleration(nopeeAccele);
			
			// Ohituksen j�lkeen palataan radalle
			while (light2.getLightValue() > blackWhiteThreshold){
				
				pilot.setTravelSpeed(10);				
				pilot.forward();

			}
		}

	}

	// Sama mutta Vasemman puolen seurannalle
	public void VasenEsteenOhitus() {
		light2.setFloodlight(true);
		LCD.clear();
		pilot.stop();
		Sound.beep();

		if(este >= esteidenLkm){

			pilot.stop();
			setRunning(false);

		} else {
			este++;
			pilot.setTravelSpeed(20);
			LCD.drawString("Ohitetaan", 0, 0);
			
			while(pilot.isMoving())Thread.yield();

			pilot.setAcceleration(hidasAccele);

			pilot.reset();
			pilot.rotate(45);
			pilot.travel(40);
			pilot.reset();
			pilot.setRotateSpeed(200);
			pilot.rotate(-90);
			pilot.setAcceleration(nopeeAccele);
		

			while (light2.getLightValue() > blackWhiteThreshold){
				pilot.setTravelSpeed(10);
				pilot.forward();
			}
		}
	}

	
	public void Pysahdy() {
		
		LCD.drawString("Pysahdy kutsuttu", 1, 1);

		Motor.B.setAcceleration(accele);
		Motor.C.setAcceleration(accele);
		Motor.C.stop();
		Motor.B.stop();
		aj.aika.reset();

	}

}
