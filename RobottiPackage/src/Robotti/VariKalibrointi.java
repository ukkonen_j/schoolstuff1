package Robotti;

import lejos.nxt.*;

public class VariKalibrointi {

	//VariKalibrointi-luokalla kalibroidaan v�rit ennen radalle menoa. 
	
	private static int valkoinen; //Valkoisen arvo on yleens� n. 47
	private static int musta; // Mustan arvo on yleens� n. 14
	private static int blackWhiteThreshold; //Mustan ja valkoisen v�li, eli "viiva", jota robotti seuraa.


	public static int getValkoinen() {
		return valkoinen;
	}


	public static void setValkoinen(int valkoinen) {
		VariKalibrointi.valkoinen = valkoinen;
	}


	public static int getMusta() {
		return musta;
	}


	public static void setMusta(int musta) {
		VariKalibrointi.musta = musta;
	}


	public static int getBlackWhiteThreshold() {
		return blackWhiteThreshold;
	}


	public static void setBlackWhiteThreshold(int blackWhiteThreshold) {
		VariKalibrointi.blackWhiteThreshold = blackWhiteThreshold;
	}



	public void Kalibrointi() {

		ColorSensor light = new ColorSensor(SensorPort.S4);

		light.setFloodlight(true);
		

			
		
		//Valkoisen m��ritys
		
		LCD.drawString("Paina ENTER", 0, 0);
		LCD.drawString("asettaaksesi", 0, 1);
		LCD.drawString("valkoisen arvon", 0, 2);

		Button.ENTER.waitForPress();
		LCD.clear();

		while(!Button.ESCAPE.isDown()){

			LCD.drawInt(light.getLightValue(), 0, 0);
			valkoinen = light.getLightValue();
			LCD.drawString("Paina Escape", 0, 1);
			LCD.drawString("hyvaksyaksesi", 0, 2);
		}


		LCD.clear();
		LCD.drawString("Valkoisen arvo:", 0, 0);
		LCD.drawInt(valkoinen, 0, 1);
		LCD.drawString("Paina ENTER", 0, 2);
		LCD.drawString("asettaaksesi", 0, 3);
		LCD.drawString("mustan arvon", 0, 4);
		
		// Mustan m��ritys

		Button.ENTER.waitForPress();
		LCD.clear();

		while(!Button.ESCAPE.isDown()){

			LCD.drawInt(light.getLightValue(), 0, 0);
			musta = light.getLightValue();
			LCD.drawString("Paina Escape", 0, 1);
			LCD.drawString("hyvaksyaksesi", 0, 2);
		}

		LCD.clear();
		LCD.drawString("Valkoisen arvo:", 0, 0);
		LCD.drawInt(valkoinen, 0, 1);
		LCD.drawString("Mustan arvo:", 0, 2);
		LCD.drawInt(musta, 0, 3);
		LCD.drawString("Paina ENTER", 0, 4);
		LCD.drawString("asettaaksesi", 0, 5);
		LCD.drawString("viivan arvon", 0, 6);
		
		Button.ENTER.waitForPress();

		//Viivan m��ritys

		LCD.clear();


		while(!Button.ESCAPE.isDown()){

			LCD.drawInt(light.getLightValue(), 0, 0);
			blackWhiteThreshold = light.getLightValue();
			LCD.drawString("Paina Escape", 0, 1);
			LCD.drawString("hyvaksyaksesi", 0, 2);
		}

		// Lopuksi tulostetaan asetetut arvot
		
		LCD.clear();
		LCD.drawString("Valkoisen arvo:", 0, 0);
		LCD.drawInt(valkoinen, 0, 1);
		LCD.drawString("Mustan arvo:", 0, 2);
		LCD.drawInt(musta, 0, 3);
		LCD.drawString("Viivan arvo:", 0, 4);
		LCD.drawInt(blackWhiteThreshold, 0, 5);
		LCD.drawString("Paina Enter", 0, 6);
		LCD.drawString("lopettaaksesi", 0, 7);
		Button.ENTER.waitForPress();
		}
	}

