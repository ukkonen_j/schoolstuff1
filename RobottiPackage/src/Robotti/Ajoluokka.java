package Robotti;



import lejos.nxt.*;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.USB;
import lejos.util.*;
import lejos.robotics.*;
import java.io.*;


public class Ajoluokka {

	public Moottori mo;
	public Kyttailija ky;
	public VariKalibrointi vk;
	public Ajastin aj;
	
	
	public Ajoluokka() {

		mo = new Moottori();
		ky = new Kyttailija();
		vk = new VariKalibrointi();
		aj = new Ajastin();

	}

	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Yhteys yhteys = new Yhteys();
		
		// Asetetaan Wallen logo
		Kuva kuva = new Kuva();
        kuva.kuvanLisays("walle.lni");
		LCD.clear();
		
		yhteys.kysy();
		// Alustetaan värikalibrointi
		VariKalibrointi vk = new VariKalibrointi();

		vk.Kalibrointi();

		// Alustetaan ja käynnistetään threadit
		
		Thread seurailija = new Thread(new Seurailija());
		Thread ajastin = new Thread(new Ajastin());
		Thread kytta = new Thread(new Kyttailija());
	
		seurailija.start();
		kytta.start();	
		ajastin.start();

                

	}



}
