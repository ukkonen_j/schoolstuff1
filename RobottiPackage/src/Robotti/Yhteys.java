package Robotti;

import lejos.nxt.*;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.USB;


import lejos.util.TextMenu;

import java.io.*;

public class Yhteys {
	
	
	
	// Yhdistetään USB
	
	public void kysy() throws Exception{
		
		// Kysytään halutaanko yhdistää USB
		
		String yesno[] = {"Kylla", "Ei"};
		TextMenu menu = new TextMenu(yesno, 1, "Yhdista USB?");
		
		int valinta = menu.select();
		
		if (valinta == 0){
			   LCD.clear();
	            LCD.drawString("Luo USB yhteys", 0, 0);
	              
	            
			NXTConnection conn = null;
			
	         
	            conn = USB.waitForConnection();
	            LCD.drawString("Yhteys luotu", 0, 2);
	           
	            LCD.clear();
	            
	            // Saadaan tiedot käyttöjärjestelmästä 
	            
	            LCD.drawString("Odotetaan", 0, 1);
	            LCD.drawString("kirjoitusta", 0, 2);
	          
	            
	            
	            Kirjoita kirjoita = new Kirjoita();

	            // Otetaan kirjoitus vastaan PC -ohjelmasta
	          
	                DataInputStream dis = conn.openDataInputStream();

	                               
	                try {
	                	// Lähetetään tiedot kirjoita -luokkaan jossa robottiin asetetaan config -tiedosto
	                	String teksti =  dis.readUTF();
	                	dis.close();
	                	
	                	
							kirjoita.kirjoitaFile(teksti);
					
	                
	                	LCD.drawString(teksti, 0, 4);
	               
	                	
	                	LCD.drawString("Kirjoitettu", 0, 5);
	                	
	        
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }

	
	                LCD.clear();
		}
	}
}
